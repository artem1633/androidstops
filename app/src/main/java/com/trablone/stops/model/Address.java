package com.trablone.stops.model;

import io.realm.RealmObject;

public class Address extends RealmObject {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLan() {
        return lan;
    }

    public void setLan(String lan) {
        this.lan = lan;
    }

    private String lat;
    private String lan;

    @Override
    public String toString() {
        return name;
    }
}
