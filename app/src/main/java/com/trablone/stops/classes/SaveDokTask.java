package com.trablone.stops.classes;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.trablone.stops.apache.WordCreator;
import com.trablone.stops.model.Ad;
import com.trablone.stops.model.Dok;
import com.trablone.stops.model.Stop;

import java.io.File;
import java.util.List;

import io.realm.Realm;
import io.realm.Sort;

public class SaveDokTask extends AsyncTask<Long, Void, Boolean> {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ProgressDialog dialog;
    private WordCreator wordCreator;
    private boolean save;

    public SaveDokTask(Context context, boolean save) {
        this.context = context;
        this.save = save;
        wordCreator = new WordCreator();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Создание файла...");
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(Long... lists) {
        if (save){

            Long date = lists[0];
            Realm realm = Realm.getDefaultInstance();

            Log.e("tr", "date:" + date);
            Stop stop = realm.where(Stop.class).equalTo("date", date).findFirst();
            List<Ad> ads = realm.where(Ad.class).equalTo("date", date).findAll();
            wordCreator.creareDocument();

            wordCreator.addStop(stop);
            removeFile(stop.getPhoto());
            for (Ad ad : ads){
                wordCreator.addAd(ad);
                removeFile(ad.getPhoto());
            }

            wordCreator.saveDocument(Utils.getDocFile(stop.getName(), stop.getDate()).getAbsolutePath());



            realm.close();
        }else {
            try {
                Thread.sleep(1000 * 60 * 60 * 24);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    private void removeFile(String patch){
        File file = new File(patch);
        file.delete();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        dialog.dismiss();
    }
}
