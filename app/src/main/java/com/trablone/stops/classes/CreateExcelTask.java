package com.trablone.stops.classes;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.trablone.stops.model.Ad;
import com.trablone.stops.model.Stop;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.Sort;

public class CreateExcelTask extends AsyncTask<Void, Void, Boolean> {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ProgressDialog dialog;

    public CreateExcelTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Создание файла...");
        dialog.show();
    }


    @Override
    protected Boolean doInBackground(Void... voids) {

        Realm realm = Realm.getDefaultInstance();
        List<Stop> stops = realm.where(Stop.class).sort("date", Sort.DESCENDING).greaterThan("all_count", 0).findAll();
        List<String> dates = new ArrayList<>();
        List<String> names = new ArrayList<>();
        for (Stop stop : stops){

            if (!dates.contains(stop.getDay())){
               dates.add(stop.getDay());
            }

            if (!names.contains(stop.getName()) && !TextUtils.isEmpty(stop.getName())){
                names.add(stop.getName());
            }
        }

        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        createAll(workbook, dates, names, realm);

        FileOutputStream fileOut = null;
        long date = System.currentTimeMillis();
        try {
            fileOut = new FileOutputStream(Utils.getExcelFile("Итоговый количественный отчет", date));
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        createContacts(workbook, realm);

        try {
            fileOut = new FileOutputStream(Utils.getExcelFile("Контакты", date));
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return true;
    }

    private static String[] columns = {"№", "Название остановки", "Дата", "Телефон", "Примечание"};


    private void createContacts(Workbook workbook, Realm realm) {
        Sheet sheet = workbook.createSheet("Контакты");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        List<Ad> ads = realm.where(Ad.class).sort("date", Sort.ASCENDING).findAll();

        int rowNum = 1;
        for (int i = 0; i < ads.size(); i++){
            Ad item = ads.get(i);
            Row row = sheet.createRow(rowNum++);
            Cell cell = row.createCell(0);
            cell.setCellValue(i+1);

            Stop stop = realm.where(Stop.class).equalTo("date", item.getDate()).findFirst();
            cell = row.createCell(1);
            cell.setCellValue(stop.getName());

            cell = row.createCell(2);
            cell.setCellValue(stop.getDay());

            cell = row.createCell(3);
            cell.setCellValue(item.getPhone());

            cell = row.createCell(4);
            cell.setCellValue(item.getDesc());
        }

    }

    private void createAll(Workbook workbook, List<String> dates, List<String> names,Realm realm){
        Sheet sheet = workbook.createSheet("Итоговый количественный отчет");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row rowSumm = sheet.createRow(0);
        // Create a Row
        Row headerRow = sheet.createRow(1);

        Cell cell = headerRow.createCell(0);
        cell.setCellValue("Дата");
        cell.setCellStyle(headerCellStyle);

        cell = headerRow.createCell(1);
        cell.setCellValue("ИТОГО");
        cell.setCellStyle(headerCellStyle);
        // Create cells

        int lastColumn = 0;

        for(int i = 0; i < names.size(); i++) {
            String stop = names.get(i);
            cell = headerRow.createCell(i + 2);
            cell.setCellValue(stop);
            cell.setCellStyle(headerCellStyle);
            if (i == names.size() - 1){
                lastColumn = cell.getColumnIndex();
            }
        }

        int rowNum = 2;
        for (String date : dates){
            Row row = sheet.createRow(rowNum++);


            Cell dateOfBirthCell = row.createCell(0);
            dateOfBirthCell.setCellValue(date);
            //dateOfBirthCell.setCellStyle(dateCellStyle);

            Cell countCell = row.createCell(1);
            String formula= "SUM("+ CellReference.convertNumToColString(countCell.getColumnIndex() + 1) + rowNum +":" + CellReference.convertNumToColString(lastColumn)+ rowNum + ")";
            countCell.setCellType(CellType.FORMULA);
            countCell.setCellFormula(formula);

            for(int i = 0; i < names.size(); i++) {
                String name = names.get(i);

                Stop item = realm.where(Stop.class)
                        .equalTo("day", date)
                        .equalTo("name", name)
                        .findFirst();
                if (item != null){
                    Cell itemCell = row.createCell(i + 2);
                    if (item.getAll_count() != 0) {
                        itemCell.setCellValue(item.getAll_count());
                    }
                }

            }
        }

        Cell countCell = rowSumm.createCell(1);
        String formula= "SUM("+ CellReference.convertNumToColString(countCell.getColumnIndex()) + 3 +":" + CellReference.convertNumToColString(countCell.getColumnIndex()) + rowNum + ")";
        countCell.setCellType(CellType.FORMULA);
        countCell.setCellFormula(formula);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        dialog.dismiss();
    }
}
