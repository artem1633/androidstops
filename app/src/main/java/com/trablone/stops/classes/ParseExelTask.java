package com.trablone.stops.classes;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trablone.stops.apache.ExcelParser;

import io.realm.Realm;

public class ParseExelTask extends AsyncTask<String, Void, Boolean> {

    @SuppressLint("StaticFieldLeak")
    private Context context;
    private ProgressDialog dialog;

    public ParseExelTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Чтение файла...");
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        Realm realm = Realm.getDefaultInstance();

        return ExcelParser.parse(realm, strings[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        dialog.dismiss();
    }
}
