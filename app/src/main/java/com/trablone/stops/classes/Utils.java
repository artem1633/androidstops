package com.trablone.stops.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;



import java.io.File;
import java.io.FileOutputStream;

import java.io.OutputStream;


public class Utils {


    public static boolean isOk(String result){
        return result.contains("ok");
    }
    public static String getMD5(String login, String pass, String firebaseId){
        StringBuilder builder = new StringBuilder();
        builder.append(login);
        builder.append(pass);
        builder.append(firebaseId);

        return md5(builder.toString());
    }

    public static int dpToPx(Context context, int px) {
        return px * (int) context.getResources().getDisplayMetrics().density;
    }

    public static int pxToDp(Context context, int dp) {
        return dp / (int)context.getResources().getDisplayMetrics().density;
    }
    public static String getHash(String txt, String hashType) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
            byte[] array = md.digest(txt.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            //error action
        }
        return null;
    }

    public static String md5(String txt) {
        return getHash(txt, "MD5");
    }




    private static void installApp(Context context,File file){

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Log.e("tr", "file open: " + file.getAbsolutePath());

        Uri photoURI = getFileUri(context, file);

        if (Build.VERSION.SDK_INT > 21) {
            //photoURI = FileProvider.getUriForFile(context, "myCRM.myCRM777", file);
            intent.setData(photoURI);
        } else {
            //photoURI = Uri.fromFile(file);
            intent.setDataAndType(photoURI, "*/*");
        }

        Log.e("tr", "uri: " + photoURI);
        context.startActivity(intent);

    }

    public static Uri getFileUri(Context context, File file){
        Uri photoURI;

        if (Build.VERSION.SDK_INT > 21) {
            photoURI = FileProvider.getUriForFile(context, context.getPackageName(), file);

        } else {
            photoURI = Uri.fromFile(file);

        }

        return photoURI;
    }


    public static File getImageFile(){

        File folder = getBaseFolder();

        File file = new File(folder, String.valueOf(System.currentTimeMillis()) + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(file, String.valueOf(System.currentTimeMillis()) + ".png");
        }

        return file;

    }

    public static String parsePhone(String phone){
        return phone.replaceAll("[^0-9-+]+", "");
    }

    public static String getParsePhone(String phone) {

        String[] phoneArray = phone.split("");
        int length = phoneArray.length;

        if (length > 12) {
            phone = phone.substring(0, 12);
            phoneArray = phone.split("");
            length = phoneArray.length;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(phoneArray[i]);
            if (i == 2) {
                builder.append(" ");
            }
            if (i == 5) {
                builder.append(" ");
            }
            if (i == 8) {
                builder.append(" ");
            }

        }
        return builder.toString();
    }

    public static File getDocFile(String name, long date){

        File folder = getBaseFolder();

        File file = new File(folder, name + "_" + date + ".docx");

        return file;

    }

    public static File getExcelFile(String name, long date){

        File folder = getBaseFolder();

        File file = new File(folder, name + "_" + date + ".xlsx");

        return file;

    }

    public static File getBaseFolder(){
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + "/Stop";
        File folder = new File(extStorageDirectory);
        if (!folder.exists()){
            folder.mkdir();
        }

        return folder;

    }

    public static File savebitmap(Bitmap bmp) {

        OutputStream outStream = null;
        // String temp = null;
        File file = getImageFile();

        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 50, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Log.e("tr", "file: " + file.getAbsolutePath());
        return file;
    }

    public static File getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        Log.e("tr", "result: " + result);
        return new File(result);
    }
}
