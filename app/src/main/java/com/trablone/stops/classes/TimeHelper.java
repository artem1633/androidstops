package com.trablone.stops.classes;

import android.content.Context;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeHelper {


    private final static long DAYS = 2;
    private final static String FILE_NAME = ".anbroib";

    public String host;
    private static boolean show_dialog;


    public static boolean isActivation(Context context) {
        boolean active =  isAllowTime(context);

        return active;
    }


    private static boolean isAllowTime(Context context) {
        File file = getBaseFile();

        if (file.exists()) {
            return isTime();
        } else {
            try {
                String date = encode(System.currentTimeMillis());
                writeStringAsFile(date, file);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return isTime();
        }
    }


    private static long getOffset() {
        try {
            long installDate = decode(readFileAsString(getBaseFile()));

            if (installDate == 0)
                return -1;

            long currentDate = System.currentTimeMillis();
            //Log.e("tr", "current date: " + currentDate);
            long offset = currentDate - installDate;
            //Log.e("tr", "offset date: " + offset);
            return offset;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static boolean isTime() {
        long days = TimeUnit.DAYS.toMillis(DAYS);
        //Log.e("tr", "2days: " + days);
        return getOffset() <= days;
    }

    private static File getBaseFile() {
        File patch = Environment.getExternalStorageDirectory();
        File file = new File(patch, FILE_NAME);
        return file;
    }

    public static void writeInstallTime(Context context, String date) {
        writeStringAsFile(date, getBaseFile());

    }

    public static long getTime(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date convertedDate = format.parse(date);
            return convertedDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }


    public static String encode(long time) throws UnsupportedEncodingException {
        byte[] data = String.valueOf(time).getBytes("UTF-8");
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        return base64;
    }

    public static long decode(String time) throws UnsupportedEncodingException {

        try {
            byte[] data = Base64.decode(time, Base64.DEFAULT);
            String text = new String(data, "UTF-8");

            return Long.parseLong(text);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return 0;
        }

    }

    private static boolean writeStringAsFile(final String fileContents, File file) {

        try {
            FileWriter out = new FileWriter(file);
            out.write(fileContents);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private static String readFileAsString(File file) {

        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader in = null;

        try {
            in = new BufferedReader(new FileReader(file));
            while ((line = in.readLine()) != null)
                stringBuilder.append(line);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }


}
