package com.trablone.stops.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.trablone.stops.R;
import com.trablone.stops.model.Ad;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class AdAdapter extends RecyclerView.Adapter<AdAdapter.ViewHolder>{

    public AdAdapter(Context context) {
        this.context = context;
        this.list = new RealmList<>();
    }

    private Context context;

    public List<Ad> getList() {
        return list;
    }

    private List<Ad> list;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ad, parent, false);
        return new ViewHolder(view);
    }

    public void addAd(Ad item){
        list.add(item);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Ad item = list.get(position);
        holder.textDesc.setText(item.getDesc());
        holder.textPhone.setText(item.getPhone());
        holder.textTitle.setText(item.getType());
        imageLoader.displayImage("file://" + item.getPhoto(), holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_image)
        ImageView imageView;
        @BindView(R.id.item_title)
        TextView textTitle;
        @BindView(R.id.item_phone)
        TextView textPhone;
        @BindView(R.id.item_desc)
        TextView textDesc;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
