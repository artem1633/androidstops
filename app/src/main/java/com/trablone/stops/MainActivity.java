package com.trablone.stops;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.trablone.stops.adapters.AdAdapter;
import com.trablone.stops.apache.WordCreator;
import com.trablone.stops.classes.CreateExcelTask;
import com.trablone.stops.classes.ParseExelTask;
import com.trablone.stops.classes.SaveDokTask;
import com.trablone.stops.classes.TimeHelper;
import com.trablone.stops.classes.Utils;
import com.trablone.stops.model.Ad;
import com.trablone.stops.model.Address;
import com.trablone.stops.model.Dok;
import com.trablone.stops.model.Stop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.item_location)
    AutoCompleteTextView editLocation;
    @BindView(R.id.item_desc)
    EditText editDesk;
    @BindView(R.id.image_stop)
    ImageView imageStop;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.create)
    Button button;

    private Realm realm;

    private final int PERMISSION_KAY = 155;
    private List<String> permissionList;
    private boolean permissionWrite;

    private ImageLoader imageLoader = ImageLoader.getInstance();

    private AdAdapter adAdapter;
    private Stop stop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        System.setProperty("javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");

        ButterKnife.bind(this);

        realm = Realm.getDefaultInstance();


        if (enablePermissions()){
            readLocations();
        }

        editLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchLocation(s.toString());

            }
        });

        adAdapter = new AdAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adAdapter);

        if (!TimeHelper.isActivation(this)){
            finish();
        }

        realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                List<Stop> list = realm.where(Stop.class).greaterThan("all_count", 0).findAll();
                if (list.size() > 0){
                    button.setVisibility(View.VISIBLE);
                }else {
                    button.setVisibility(View.GONE);
                }
            }
        });

        realm.beginTransaction();
        stop = realm.createObject(Stop.class);

        Calendar calendar = Calendar.getInstance();

        stop.setDate(calendar.getTime().getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Log.e("tr", "time : " + dateFormat.format(calendar.getTime()));
        stop.setDay(dateFormat.format(calendar.getTime()));


        realm.commitTransaction();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, "Обновить локации");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case 1:
                showAttach();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("StaticFieldLeak")
    private void searchLocation(final String s){

        final List<Address> list = realm.where(Address.class).contains("name", s).findAll();

        String[] arrays = new String[list.size()];
        for (int i = 0; i < list.size(); i++){
            arrays[i] = list.get(i).toString();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, arrays);

        editLocation.setAdapter(adapter);

        editLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Address address = list.get(position);
                realm.beginTransaction();
                stop.setName(address.getName());
                stop.setLan(address.getLan());
                stop.setLat(address.getLat());
                realm.commitTransaction();
            }
        });

    }

    private void readLocations(){
        List<Address> locations = realm.where(Address.class).findAll();
        if (locations == null || locations.size() == 0){
            showAttach();
        }
    }

    private void showAttach(){

        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(500)
                .withFilterDirectories(true)
                .withHiddenFiles(true)
                .start();
    }

    private File file;

    @SuppressLint("StaticFieldLeak")
    @OnClick(R.id.item_add)
    public void saveDoc(){
        if (editLocation.getText().length() == 0){
            editLocation.setError("Выберите локацию");
            return;
        }

        if (editDesk.getText().length() == 0){
            editDesk.setError("Введите площадь");
            return;
        }

        Dok dok = new Dok();
        realm.beginTransaction();
        stop.setDesc(editDesk.getText().toString());
        stop.setAll_count(adAdapter.getItemCount());
        stop.setPrivate_count(stop.getAll_count() - stop.getPay_count());

        dok.setStop(stop);
        realm.commitTransaction();
        new SaveDokTask(this, TimeHelper.isActivation(this)){
            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean){
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }.execute(stop.getDate());
    }

    @SuppressLint("StaticFieldLeak")
    @OnClick(R.id.create)
    public void create(){
        new CreateExcelTask(this){
            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                realm.beginTransaction();
                realm.delete(Ad.class);
                realm.delete(Stop.class);
                realm.commitTransaction();
            }
        }.execute();
    }

    @OnClick(R.id.image_stop)
    public void addFile(){
        try {

            file = Utils.getImageFile();

            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePhotoIntent.putExtra( MediaStore.EXTRA_OUTPUT, Utils.getFileUri(this, file));
            Intent chooserIntent = Intent.createChooser(takePhotoIntent, "Выберите приложение");

            startActivityForResult(chooserIntent, 400);
        } catch (ActivityNotFoundException e) {

        }
    }
    @OnClick(R.id.item_ad)
    public void addAd(){

        if (editLocation.getText().length() == 0){
            Toast.makeText(this, "Выберите локацию", Toast.LENGTH_SHORT).show();
            return;
        }
        try {

            file = Utils.getImageFile();

            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePhotoIntent.putExtra( MediaStore.EXTRA_OUTPUT, Utils.getFileUri(this, file));
            Intent chooserIntent = Intent.createChooser(takePhotoIntent, "Выберите приложение");


            startActivityForResult(chooserIntent, 300);
        } catch (ActivityNotFoundException e) {

        }
    }

    private boolean enablePermissions() {
        permissionList = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.CAMERA);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionWrite = false;
        } else {
            permissionWrite = true;
        }

        int size = permissionList.size();
        String[] permissions = new String[size];
        for (int i = 0; i < permissionList.size(); i++) {
            permissions[i] = permissionList.get(i);
        }

        if (size > 0) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_KAY);
            return false;
        }


        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0){
            readLocations();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {

                switch (requestCode) {
                    case 500:
                        realm.beginTransaction();
                        realm.delete(Address.class);
                        realm.commitTransaction();

                        String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                        Log.e("tr", "file: " + filePath);
                        parseFile(filePath);
                        break;
                    case 400:
                        realm.beginTransaction();
                        stop.setPhoto(file.getAbsolutePath());
                        realm.commitTransaction();
                        imageLoader.displayImage("file://" + file.getAbsolutePath(), imageStop);
                        break;
                    case 300:
                        showAdDialog(file.getAbsolutePath(), null, null, false);
                        break;
                }


            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private String[] types = new String[]{"Частное объявление", "Коммерческое объявление"};
    private int oldSelection = -1;
    private boolean ignoreTextChange = false;
    private void showAdDialog(final String file, String desc, String phone, boolean error){
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_add, null, false);
        final Spinner spinner = view.findViewById(R.id.item_title);
        final EditText editPhone = view.findViewById(R.id.item_phone);
        final EditText editDesc = view.findViewById(R.id.item_desc);
        ImageView imageView = view.findViewById(R.id.item_image);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, types);
        spinner.setAdapter(adapter);

        imageLoader.displayImage("file://" + file, imageView);

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!ignoreTextChange) {
                    ignoreTextChange = true;

                    int selection = editPhone.getSelectionStart();

                    if (oldSelection > 0 && oldSelection < selection) {
                        String number = s.toString();
                        number = number.substring(0, selection);
                        String phoneNumber = Utils.parsePhone(number);
                        number = Utils.getParsePhone(phoneNumber);
                        editPhone.setText(number);
                        selection = editPhone.getText().length();
                        editPhone.setSelection(selection);
                    }
                    oldSelection = selection;
                    ignoreTextChange = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editPhone.setText("+7");

        if (error){
            editDesc.setText(desc);
            if (TextUtils.isEmpty(desc))
                editDesc.setError("Введите описание");
            editPhone.setText(phone);
            if (TextUtils.isEmpty(phone) || phone.length() < 15)
                editPhone.setError("Введите телефон");
        }

        new AlertDialog.Builder(this)
                .setView(view)
                .setPositiveButton("Добавить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String desc = editDesc.getText().toString();
                        String phone = editPhone.getText().toString();

                        if (!TextUtils.isEmpty(desc) && !TextUtils.isEmpty(phone) && phone.length() == 15){
                            realm.beginTransaction();
                            Ad item = realm.createObject(Ad.class);
                            item.setDesc(desc);
                            item.setPhone(phone);
                            item.setPhoto(file);
                            item.setType(spinner.getSelectedItem().toString());
                            item.setDate(stop.getDate());
                            if (spinner.getSelectedItemPosition() == 1) {
                                stop.setPay_count(stop.getPay_count() + 1);
                            }
                            realm.commitTransaction();
                            adAdapter.addAd(item);
                        }else {
                            showAdDialog(file, desc, phone, true);
                        }

                    }
                })
                .setNegativeButton("Отмена", null)
                .show();

    }


    @SuppressLint("StaticFieldLeak")
    private void parseFile(String file){
        new ParseExelTask(this){
            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

            }
        }.execute(file);
    }
}
