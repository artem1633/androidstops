package com.trablone.stops.apache;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Dimension;

import com.trablone.stops.model.Ad;
import com.trablone.stops.model.Stop;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class WordCreator {


    public Bitmap getImageDimension(File imgFile){
        return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
    }

    private XWPFDocument docxModel;

    public void creareDocument() {

        docxModel = new XWPFDocument();
        //CTSectPr ctSectPr = docxModel.getDocument().getBody().addNewSectPr();
        // получаем экземпляр XWPFHeaderFooterPolicy для работы с колонтитулами
        //XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(docxModel, ctSectPr);

        // создаем верхний колонтитул Word файла
        //CTP ctpHeaderModel = createHeaderModel("Верхний колонтитул - создано с помощью Apache POI на Java :)");
        // устанавливаем сформированный верхний
        // колонтитул в модель документа Word
        //XWPFParagraph headerParagraph = new XWPFParagraph(ctpHeaderModel, docxModel);
        //headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT, new XWPFParagraph[]{headerParagraph});

        // создаем нижний колонтитул docx файла
        //CTP ctpFooterModel = createFooterModel("Просто нижний колонтитул");
        // устанавливаем сформированый нижний
        // колонтитул в модель документа Word
        //XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooterModel, docxModel);
        //headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, new XWPFParagraph[]{footerParagraph});

        // создаем обычный параграф, который будет расположен слева,
        // будет синим курсивом со шрифтом 25 размера


        // сохраняем модель docx документа в файл

    }

    public void saveDocument(String patch) {
        try {

            FileOutputStream outputStream = new FileOutputStream(patch);
            docxModel.write(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addStop(Stop stop) {


        XWPFParagraph bodyParagraph = docxModel.createParagraph();

        bodyParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun paragraphConfig = bodyParagraph.createRun();
        //paragraphConfig.setItalic(true);
        paragraphConfig.setFontSize(18);
        // HEX цвет без решетки #


        //paragraphConfig.setColor("06357a");

        paragraphConfig.setText(stop.getName());
        paragraphConfig.addBreak();
        paragraphConfig.setText(stop.getLat() + " " + stop.getLan());
        paragraphConfig.addBreak();

        addImage(stop.getPhoto(), paragraphConfig);
        paragraphConfig.addBreak();


        bodyParagraph = docxModel.createParagraph();

        bodyParagraph.setAlignment(ParagraphAlignment.LEFT);
        paragraphConfig = bodyParagraph.createRun();
        //paragraphConfig.setItalic(true);
        paragraphConfig.setFontSize(18);
        paragraphConfig.setText(stop.getDesc());
        paragraphConfig.addBreak();
        paragraphConfig.setText("Итого не санкционировано размещено " + stop.getAll_count() + " объявления");
        paragraphConfig.addBreak();
        paragraphConfig.setText("Из них частных объявлений " + stop.getPrivate_count() + " штук");
        paragraphConfig.addBreak();
        paragraphConfig.setText("коммерческих объявлений " + stop.getPay_count() + " штук");
        paragraphConfig.addBreak();


    }

    private void addImage(String patch, XWPFRun paragraphConfig){

        InputStream pic = null;
        try {
            pic = new FileInputStream(patch);
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        Bitmap bitmap = getImageDimension(new File(patch));
        double width = bitmap.getWidth();
        double height = bitmap.getHeight();

        double scaling = 1.0;
        if (width > 72*6)
            scaling = (72*6)/width; //scale width not to be greater than 6 inches


        try {
            paragraphConfig.addPicture(pic, XWPFDocument.PICTURE_TYPE_JPEG, patch, Units.toEMU(width * scaling), Units.toEMU(height * scaling));
            pic.close();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addAd(Ad ad) {


        XWPFParagraph bodyParagraph = docxModel.createParagraph();

        bodyParagraph.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun paragraphConfig = bodyParagraph.createRun();
        //paragraphConfig.setItalic(true);
        paragraphConfig.setFontSize(18);
        // HEX цвет без решетки #


        //paragraphConfig.setColor("06357a");

        paragraphConfig.setText(ad.getType());
        paragraphConfig.addBreak();
        paragraphConfig.setText(ad.getDesc());
        paragraphConfig.addBreak();
        paragraphConfig.setText(ad.getPhone());
        paragraphConfig.addBreak();

        addImage(ad.getPhoto(), paragraphConfig);

    }

    private static CTP createFooterModel(String footerContent) {
        // создаем футер или нижний колонтитул
        CTP ctpFooterModel = CTP.Factory.newInstance();
        CTR ctrFooterModel = ctpFooterModel.addNewR();
        CTText cttFooter = ctrFooterModel.addNewT();

        cttFooter.setStringValue(footerContent);
        return ctpFooterModel;
    }

    private static CTP createHeaderModel(String headerContent) {
        // создаем хедер или верхний колонтитул
        CTP ctpHeaderModel = CTP.Factory.newInstance();
        CTR ctrHeaderModel = ctpHeaderModel.addNewR();
        CTText cttHeader = ctrHeaderModel.addNewT();

        cttHeader.setStringValue(headerContent);
        return ctpHeaderModel;
    }
}
