package com.trablone.stops.apache;

import android.util.Log;

import com.trablone.stops.model.Address;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import io.realm.Realm;

public class ExcelParser {

    public static boolean parse(Realm realm, String fileName) {

        InputStream inputStream;

        Workbook workBook = null;
        try {

            inputStream = new FileInputStream(fileName);

            if (fileName.toLowerCase().endsWith("xlsx")) {

                workBook = new XSSFWorkbook(inputStream);

            } else if (fileName.toLowerCase().endsWith("xls")) {

                workBook = new HSSFWorkbook(inputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int numberOfSheets = workBook.getNumberOfSheets();

        realm.beginTransaction();

        for(int i = 0; i < numberOfSheets; i++){
            Sheet sheet = workBook.getSheetAt(i);
            Iterator<Row> it = sheet.iterator();

            while (it.hasNext()) {

                Row row = it.next();
                Iterator<Cell> cells = row.iterator();

                Address item = realm.createObject(Address.class);

                int position = 0;
                while (cells.hasNext()) {
                    Cell cell = cells.next();
                    int cellType = cell.getCellType();

                    switch (cellType) {
                        case Cell.CELL_TYPE_STRING:
                            Log.e("tr", "position: " + position + " data: " + cell.getStringCellValue());
                            if (position == 0){
                                position++;
                                item.setName(cell.getStringCellValue().toLowerCase());
                            }else if (position == 1){
                                position++;
                                item.setLan(cell.getStringCellValue());
                            }else if (position == 2){
                                position = 0;
                                item.setLat(cell.getStringCellValue());
                            }

                            break;

                        default:
                            //result += "|";
                            break;
                    }
                }

            }
        }

        realm.commitTransaction();


        return true;
    }
}
